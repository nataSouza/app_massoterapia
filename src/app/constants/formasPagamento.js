export default [
    {
        nome: 'Master Card',
        imagem: 'https://www.mastercard.com.br/content/dam/mccom/global/logos/logo-mastercard-mobile.svg'
    },
    {
        nome: 'Visa',
        imagem: 'https://www.comoinvestirnoexterior.com/wp-content/uploads/2019/05/visa.jpg'
    },
    {
        nome: 'Boleto',
        imagem: 'https://medpri.me/app/img/icons/icon-boleto.png'
    }
]
