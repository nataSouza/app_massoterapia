import localKeys from '../constants/localKeys';

export default {
    save: usuario => {
        localStorage.setItem(localKeys.AUTH,JSON.stringify(usuario));
    },

    get: () => {
        const authenticatedUser = localStorage.getItem(localKeys.AUTH);
        if(authenticatedUser){
            return JSON.parse(authenticatedUser);
        }
        return null;
    },

    logout: () => {
        localStorage.clear();
    }
}