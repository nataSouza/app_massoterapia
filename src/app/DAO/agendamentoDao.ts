import localKeys from '../constants/localKeys';

export default {
    async save(agendamento) {
        const agendamentos = await this.get(agendamento.usuario.id);
        agendamentos.push(agendamento);
        const salved = await localStorage.setItem(localKeys.AGENDAMENTO,JSON.stringify(agendamentos));

        return salved;
    },

    async get(idLoggeduser) {
        const agendamentos = await localStorage.getItem(localKeys.AGENDAMENTO);
        return agendamentos ? JSON.parse(agendamentos).filter(agendamento => agendamento.usuario.id == idLoggeduser) : [];
    }
}
