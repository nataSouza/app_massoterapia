import { Component, OnInit } from '@angular/core';
import Auth from 'src/app/middlewares/Auth';
import { NavController } from '@ionic/angular';
import Agendamento from 'src/app/models/Agendamento';
import agendamentoDao from 'src/app/DAO/agendamentoDao';
import { AgendamentoService } from 'src/app/services/agendamento.service';

@Component({
  selector: 'app-meus-agendamentos',
  templateUrl: './meus-agendamentos.page.html',
  styleUrls: ['./meus-agendamentos.page.scss'],
})
export class MeusAgendamentosPage extends Auth implements OnInit {


  agendamentos: Agendamento[];

  constructor(public navCtrl: NavController, private agendamentoService: AgendamentoService) {
    super(navCtrl);
  }

  ngOnInit() {
    this.populateAgendamentos();
  }

  async populateAgendamentos() {

    this.agendamentoService.alteracaoLista.subscribe((novaLista :Agendamento[]) => this.agendamentos = novaLista)

    this.agendamentos = await agendamentoDao.get(this.userLogged.id);
  }

  go(route) {
    this.navCtrl.navigateForward(route);
  }
}
