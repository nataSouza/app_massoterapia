import { Component, OnInit } from '@angular/core';
import Auth from 'src/app/middlewares/Auth';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Usuario from 'src/app/models/Usuario';
import authHelper from 'src/app/helpers/authHelper';
import usuarioDao from 'src/app/DAO/usuarioDao';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage extends Auth implements OnInit {

  formGroup: FormGroup;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder) {
    super(navCtrl);
    
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      nome: [this.userLogged.nome, Validators.required],
      email: [this.userLogged.email, Validators.required],
      endereco: [this.userLogged.endereco],
    })
  }

  ngOnInit() {
    this.createForm();
  }

  save() {
    const usuario: Usuario = {
      ...this.formGroup.value,
      image: this.userLogged.image
    }

    usuarioDao.save(usuario);

    this.navCtrl.back();
  }
  getUser(){
    return this.userLogged;
  }
}
