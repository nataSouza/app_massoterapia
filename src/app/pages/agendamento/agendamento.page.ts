import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController, NavController } from '@ionic/angular';
import Agendamento from 'src/app/models/Agendamento';
import agendamentoDao from 'src/app/DAO/agendamentoDao';
import Profissional from 'src/app/models/Profissional';
import { Observable } from 'rxjs';
import { LoadMockService } from 'src/app/services/load-mock.service';
import Servicos from 'src/app/models/Servico';
import Auth from 'src/app/middlewares/Auth';
import { NavigationExtras, Router } from '@angular/router';
import FormaPagamento from 'src/app/models/FormaPagamento';


@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.page.html',
  styleUrls: ['./agendamento.page.scss'],
})
export class AgendamentoPage extends Auth implements OnInit {

  formGroup: FormGroup;

  profissionais: Profissional[];
  servicos: Servicos[];
  formasPagamento: FormaPagamento[];

  constructor(public formBuilder: FormBuilder,public toastController: ToastController,private loadMockService: LoadMockService, public navCtrl: NavController, private router: Router) {
    super(navCtrl);

    this.loadMock();
    this.formGroup = this.formBuilder.group({
      servico: ['', Validators.required],
      profissional: ['', Validators.required],
      dataAgendamento: ['', Validators.required],
      horaAgendamento: ['', Validators.required],
    })
  }

  loadMock() {
    this.loadMockService.getProfissionais().subscribe(res => this.profissionais = res);

    this.loadMockService.getServicos().subscribe(res => this.servicos = res);
  }

  getUser() {
    return this.userLogged;
  }

  async showError() {
    const toast = await this.toastController.create({
      message: 'Preencha os campos corretos',
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {
  }

  async save() {
    if(!this.formGroup.valid){
      this.showError();
      return;
    }
    console.log(this.profissionais);

    const agendamento: Agendamento = {
      ...this.formGroup.value,
      usuario: this.userLogged
    }

    let navigationExtras: NavigationExtras = {
      queryParams: {
        agendamento: JSON.stringify(agendamento)
      }
    };
    
    this.router.navigate(['/pagamento'],navigationExtras);

  }

}
