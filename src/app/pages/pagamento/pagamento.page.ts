import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Auth from 'src/app/middlewares/Auth';
import { NavController, ToastController } from '@ionic/angular';
import Agendamento from 'src/app/models/Agendamento';
import agendamentoDao from 'src/app/DAO/agendamentoDao';
import { LoadMockService } from 'src/app/services/load-mock.service';
import FormaPagamento from 'src/app/models/FormaPagamento';
import { AgendamentoService } from 'src/app/services/agendamento.service';

@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.page.html',
  styleUrls: ['./pagamento.page.scss'],
})
export class PagamentoPage extends Auth implements OnInit {

  formaPagamento: number;

  agendamento:Agendamento;

  formasPagamento: FormaPagamento[];

  masterCardImage = "https://www.mastercard.com.br/content/dam/mccom/global/logos/logo-mastercard-mobile.svg";
  visaImage = "https://www.comoinvestirnoexterior.com/wp-content/uploads/2019/05/visa.jpg";
  boletoImage = "https://medpri.me/app/img/icons/icon-boleto.png";

  constructor(private route: ActivatedRoute, private router: Router, public navCtrl: NavController, private toastController: ToastController,private loadMockService: LoadMockService, private agendamentoService: AgendamentoService) {
    super(navCtrl);
    this.route.queryParams.subscribe(params => {
      if (params && params.agendamento) {
        this.agendamento = JSON.parse(params.agendamento);
      }
    });

    this.loadMock();
  }

  ngOnInit() {
  }

  loadMock() {
    this.loadMockService.getFormasPagamento().subscribe(res => this.formasPagamento = res);
  }

  getFormaPagamento = (id: number) => this.formasPagamento.find(formaPagamento => formaPagamento.id == id);
  

  async toast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  agendar() {
    if(!this.formaPagamento){
      this.toast("Escolha a forma de pagamento");
      return;
    }

    try{
      
      this.agendamento.formaPagamento = this.getFormaPagamento(this.formaPagamento);
    
      this.agendamentoService.save(this.agendamento,this.userLogged.id);
      this.toast("Massagem agendada !");
      this.navCtrl.navigateRoot('/meus-agendamentos');
    }catch(err){
      this.toast("Houve um problema ao salvar. Tente novamente.");
    }
  }
}
