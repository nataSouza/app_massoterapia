import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import authHelper from 'src/app/helpers/authHelper';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import Usuario from 'src/app/models/Usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {

  constructor(private afAuth: AngularFireAuth, private navCtrl: NavController, private authService: AuthService) {
    
  }

  ngOnInit() {
    if(authHelper.isLogged()){
      this.navCtrl.navigateRoot('/meus-agendamentos');
    }
  }

  facebookAuthentication() {
    this.AuthLogin(new auth.FacebookAuthProvider());
  }

  AuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
    .then(user => {
      const loggedUser: Usuario = this.getInstanceUser(user);

      this.authService.login(loggedUser);
      
      this.navCtrl.navigateRoot('/meus-agendamentos');
    }).catch((error) => {
      console.log(error)
    })
  }

  getInstanceUser(user): Usuario {
    return {
      id: user.additionalUserInfo.profile.id,
      nome: user.additionalUserInfo.profile.name,
      email: user.additionalUserInfo.profile.email,
      image: user.additionalUserInfo.profile.picture.data.url
    }
  }
}
