export default interface Usuario {
    id: number,
    nome: string;
    image: string;
    email: string;
    endereco? : string;
}