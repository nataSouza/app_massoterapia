import Usuario from './Usuario';
import FormaPagamento from './FormaPagamento';

export default interface Agendamento {
    servico: string;
    profissional: string;
    dataAgendamento: string;
    horaAgendamento: string;
    usuario: Usuario;
    formaPagamento?: FormaPagamento;
}