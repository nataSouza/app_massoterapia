export default interface FormaPagamento {
    id: number,
    nome: string,
    imagem: string
}