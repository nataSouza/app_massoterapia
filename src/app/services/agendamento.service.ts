import { Injectable } from '@angular/core';
import localKeys from '../constants/localKeys';
import { Subject } from 'rxjs';
import agendamentoDao from '../DAO/agendamentoDao';
import Auth from '../middlewares/Auth';

@Injectable({
  providedIn: 'root'
})
export class AgendamentoService {

  alteracaoLista = new Subject();

  constructor() {
  }

  async save(agendamento,idUsuarioLogado) {
    await agendamentoDao.save(agendamento);

    const novaLista = await agendamentoDao.get(idUsuarioLogado);

    this.alteracaoLista.next(novaLista);
  }
}
