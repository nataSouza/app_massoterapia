import { Injectable } from '@angular/core';
import Profissional from '../models/Profissional';
import Servico from '../models/Servico';
import FormaPagamento from '../models/FormaPagamento';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoadMockService {

  private profissionaisUrl: string = "assets/mock/profissionais.json"
  private servicosUrl: string = "assets/mock/servicos.json"
  private formasPagamentoUrl: string = "assets/mock/formasPagamento.json"

  constructor(private http: HttpClient) { }

  getProfissionais(): Observable<Profissional[]> {
    return this.http.get<Profissional[]>(this.profissionaisUrl)
  }
  
  getServicos(): Observable<Servico[]> {
    return this.http.get<Servico[]>(this.servicosUrl)
  }

  getFormasPagamento(): Observable<FormaPagamento[]> {
    return this.http.get<FormaPagamento[]>(this.formasPagamentoUrl) 
  }

}