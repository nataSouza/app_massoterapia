import { TestBed } from '@angular/core/testing';

import { LoadMockService } from './load-mock.service';

describe('LoadMockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadMockService = TestBed.get(LoadMockService);
    expect(service).toBeTruthy();
  });
});
