import { Injectable } from '@angular/core';
import usuarioDao from '../DAO/usuarioDao';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  alteracaoUsuario = new Subject();

  constructor() {
  }

  async login(usuario) {
    await usuarioDao.save(usuario);

    const dadosUser = await usuarioDao.get();

    this.alteracaoUsuario.next(dadosUser);
  }
}
