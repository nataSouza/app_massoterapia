import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AgendamentoItemComponent } from './agendamento-item/agendamento-item.component';



@NgModule({
  declarations: [ToolbarComponent,AgendamentoItemComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ToolbarComponent,AgendamentoItemComponent]
})
export class ComponentModule { }
