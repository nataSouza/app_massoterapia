import { Component, OnInit, Input } from '@angular/core';
import authHelper from 'src/app/helpers/authHelper';
import { ModalController, AlertController, NavController } from '@ionic/angular';
import Auth from 'src/app/middlewares/Auth';
import { AuthService } from 'src/app/services/auth.service';
import Usuario from 'src/app/models/Usuario';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent extends Auth implements OnInit {

  authenticatedUser: any;

  imageUser: string = "https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif";
  loading = "https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif"
  @Input() name: string;
  constructor(public alertCtrl: AlertController, public navCtrl: NavController, private authService: AuthService) {
    super(navCtrl);
  }



  async presentPrompt() {
    const alert = await this.alertCtrl.create({
      subHeader: this.userLogged.nome,
      message: 'Ações',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Meu Perfil',
          handler: () => {
            this.navCtrl.navigateForward('/perfil');
          }
        }, {
          text: 'Deslogar',
          handler: () => {
            authHelper.logout();
            this.navCtrl.navigateRoot('/authentication');
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {    
    this.authService.alteracaoUsuario.subscribe((usuario :Usuario) => {
      this.authenticatedUser = usuario;
      this.imageUser = usuario.image;
    });
    
    if(this.userLogged && this.userLogged.image){
      console.log("Carregando imagem",this.userLogged);
      this.imageUser = this.userLogged.image;
    }

    if(this.imageUser == null){
      this.imageUser = this.loading;
    }
  }

}
