import { Component, OnInit, Input } from '@angular/core';
import Agendamento from 'src/app/models/Agendamento';

import dateHelper from '../../helpers/dateHelper.js';
 
@Component({
  selector: 'agendamento-item',
  templateUrl: './agendamento-item.component.html',
  styleUrls: ['./agendamento-item.component.scss'],
})
export class AgendamentoItemComponent implements OnInit {

  @Input() agendamento : Agendamento;

  constructor() { }

  dataAgendamento() {
    return dateHelper.formatDate(this.agendamento.dataAgendamento);
  }
  horaAgendamento() {
    return dateHelper.formatTime(this.agendamento.horaAgendamento);
  }

  ngOnInit() {}

}
