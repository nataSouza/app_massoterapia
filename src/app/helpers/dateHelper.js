

export default {
    formatDate: date => {
        if(!date) return null;
        
        const dateObj = new Date(date);
        let day = dateObj.getDate();
        let monthIndex = dateObj.getMonth();
        let year = dateObj.getFullYear();
        
        return `${day}/${monthIndex}/${year}`;
    },
    formatTime: time => {
        if(!time) return null;
        
        const timeObj = new Date(time);
        let hour = timeObj.getHours();
        let minute = timeObj.getMinutes();

        if(minute < 10){
            minute = `0${minute}`;
        }
        
        return `${hour}:${minute}`;
    }
}