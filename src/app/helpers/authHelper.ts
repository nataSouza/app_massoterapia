import localKeys from '../constants/localKeys';
import usuarioDao from '../DAO/usuarioDao';

export default {
    saveAuthentication: auth => {
        usuarioDao.save(auth);
        // localStorage.setItem(localKeys.AUTH,JSON.stringify(auth));
    },

    isLogged:() => {
        const user_auth = localStorage.getItem(localKeys.AUTH)
        return user_auth != null;
    },

    getAuthenticatedUser: () => {
        return usuarioDao.get();
    },

    logout: () => {
        localStorage.removeItem(localKeys.AUTH);
    }
}