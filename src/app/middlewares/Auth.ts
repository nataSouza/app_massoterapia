import Usuario from '../models/Usuario';
import authHelper from '../helpers/authHelper';
import { NavController } from '@ionic/angular';

export default class Auth {
    protected userLogged: Usuario;

    navCtrl: NavController;
    constructor(navCtrl: NavController) {
        this.navCtrl = navCtrl;
        this.userLogged = {
            id: 0,
            nome: '',
            email: '',
            image: '',
        }
        this.loadUserLogged();
    }

    loadUserLogged() {
        const dataFacebookLogin = authHelper.getAuthenticatedUser();
        console.log(dataFacebookLogin);
        if(!dataFacebookLogin){
            this.navCtrl.navigateRoot('/authentication');
            return;
        }

        console.log("Dados Carregados");
        this.userLogged = dataFacebookLogin;
    }
}